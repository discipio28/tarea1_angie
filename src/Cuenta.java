import java.time.LocalDate;

public class Cuenta {
    private long numCuenta;
    private double saldo;
    private LocalDate fechaApertura;
    private Cliente duennoCuenta;

    public Cuenta() {

    }

    public Cuenta(long numCuenta, double saldo, LocalDate fechaApertura, Cliente duennoCuenta) {
        this.numCuenta = numCuenta;
        this.saldo = saldo;
        this.fechaApertura = fechaApertura;
        this.duennoCuenta = duennoCuenta;
    }

    public Cuenta(int numCuenta, double saldo) {
        this.numCuenta = numCuenta;
        this.saldo = saldo;
    }

    public Cuenta(int numCuenta, Cliente duennoCuenta) {
        this.numCuenta = numCuenta;
        this.duennoCuenta = duennoCuenta;
    }

    public LocalDate getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(LocalDate fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public Cliente getDuennoCuenta() {
        return duennoCuenta;
    }

    public void setDuennoCuenta(Cliente duennoCuenta) {
        this.duennoCuenta = duennoCuenta;
    }

    public long getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(long numCuenta) {
        this.numCuenta = numCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Cuenta{" +
                "numCuenta=" + numCuenta +
                ", saldo=" + saldo +
                ", fechaApertura=" + fechaApertura +
                ", duennoCuenta=" + duennoCuenta +
                '}';
    }
}