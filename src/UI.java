import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class UI {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public static void main(String[] args) throws IOException {
        int opcion = -1;
        do {
            menu();
            opcion = seleccionarOpcion();
            elegirOpcion(opcion);
        } while (opcion!=7);
    }

    public static void menu () {
        System.out.println("-------------------------------");
        System.out.println("       Los lavadores.com       ");
        System.out.println("-------------------------------");
        System.out.println("\n1. Registrar clientes");
        System.out.println("2. Listar clientes");
        System.out.println("3. Ingresar al sistema");
        System.out.println("4. Salir");
    }

    public static void elegirOpcion (int eOpcion) throws IOException {

        switch (eOpcion) {
            case 1:
                registrarClientes();
                break;

            case 2:
                listarClientes();
                break;

            case 3:
                ingresoCliente();
                break;

            case 4:
                System.out.println("Saliendo del sistema...");
                break;

            default:
                System.out.print("Opción inválida. Por favor seleccione una opción del 1-4 listada en el menú");
                break;
        }
    }

    static int seleccionarOpcion() throws IOException {
        out.print("\nElija una opción: ");
        return Integer.parseInt(in.readLine());
    }

    static void registrarClientes() throws IOException {
        String nombre, fechaNacimiento, direccion;
        long identificacion;
        int edad;

        out.println("--------------------------------------");
        out.println("-------  REGISTRO DE CLIENTES  -------");
        out.println("--------------------------------------");

        out.print("\nIngrese el nombre del cliente: ");
        nombre = in.readLine();

        out.print("Ingrese la fecha de nacimiento del cliente: ");
        fechaNacimiento = in.readLine();

        out.print("Ingrese el número de identificación del cliente: ");
        identificacion = Long.parseLong(in.readLine());

        out.print("Ingrese la edad del cliente: ");
        edad = Integer.parseInt(in.readLine());

        out.print("Ingrese la dirección del cliente: ");
        direccion = in.readLine();

        String mensaje = BL.registrarClientes(nombre, identificacion, fechaNacimiento, edad, direccion);
        System.out.println(mensaje);

    }

    static void listarClientes() throws IOException {
        ArrayList<Cliente> listaClientes = BL.listarClientes();

        out.println("-------------------------------------");
        out.println("--------  LISTA DE CLIENTES  --------");
        out.println("-------------------------------------");

        for (int i = 0; i < listaClientes.size(); i++) {
            Cliente c;
            c = listaClientes.get(i);

            out.println("\nNombre del cliente: " + c.getNombre());
            out.println("Fecha de nacimiento del cliente: " + c.getFechaNacimiento());
            out.println("Número de identificación del cliente: " + c.getIdentificacion());
            out.println("Edad del cliente: " + c.getEdad());
            out.println("Dirección del cliente: " + c.getDireccion());
        }
    }

    static void ingresoClienteMenu() throws IOException {
        out.println("----------------------------");
        out.println("-------     MENU     -------");
        out.println("----------------------------");

        out.println("\n1. Crear una cuenta");
        out.println("2. Realizar un depósito");
        out.println("3. Realizar un retiro");
        out.println("4. Mostrar saldo");
        out.println("5. Regresar al menú principal");
    }

    static void crearCuentaMenu() throws IOException {
        System.out.println("-------------------------------");
        System.out.println("       APERTURA DE CUENTA      ");
        System.out.println("-------------------------------");
        System.out.println("\n1. Cuenta corriente");
        System.out.println("2. Cuenta de ahorro");
        System.out.println("3. Cuenta de ahorro programado");
    }

    static void depositoMenu() throws IOException {
        System.out.println("-------------------------------");
        System.out.println("             DEPÓSITOS         ");
        System.out.println("-------------------------------");
        System.out.println("\n1. Depósito a cuenta corriente o cuenta de ahorro");
        System.out.println("2. Depósito automático a cuenta de ahorro programado");
    }

    static void retiroMenu() throws IOException {
        System.out.println("-------------------------------");
        System.out.println("             RETIROS           ");
        System.out.println("-------------------------------");
        System.out.println("\n1. Retiro fondos fondos de cuenta corriente o cuenta de ahorro");
        System.out.println("2. Retiro de fondos de cuenta de ahorro programado");
    }

    static void crearCorriente(long identificacion) throws IOException {
        long numCuenta;
        double saldo;
        LocalDate fechaApertura = LocalDate.now();

        out.println("--------------------------------------");
        out.println("---  APERTURA DE CUENTA CORRIENTE  ---");
        out.println("--------------------------------------");

        out.print("\nIngrese un número de 7 dígitos: ");
        numCuenta = Long.parseLong(in.readLine());

        out.print("Ingrese el saldo a depositar en la cuenta. El saldo inicial debe ser igual o mayor de 50.000 colones: ");
        saldo = Double.parseDouble(in.readLine());

        String mensaje = BL.crearCorriente(numCuenta, saldo, identificacion, fechaApertura);
        System.out.println(mensaje);
    }

    static void crearAhorro(long identificacion) throws IOException {
        long numCuenta;
        double saldo;
        LocalDate fechaApertura = LocalDate.now();

        out.println("--------------------------------------");
        out.println("---  APERTURA DE CUENTA DE AHORRO  ---");
        out.println("--------------------------------------");

        out.print("\nIngrese un número de 7 dígitos: ");
        numCuenta = Long.parseLong(in.readLine());

        out.print("Ingrese el saldo a depositar en la cuenta. El saldo inicial debe ser igual o mayor de 50.000 colones: ");
        saldo = Double.parseDouble(in.readLine());

        String mensaje = BL.crearAhorro(numCuenta, saldo, identificacion, fechaApertura);
        System.out.println(mensaje);
    }

    static void crearAhorroProg() throws IOException {
        long numCuenta;
        long numCorriente;
        double montoDebitar;
        double saldo = 0.0;
        LocalDate fechaApertura = LocalDate.now();

        out.println("-------------------------------------------");
        out.println("- APERTURA DE CUENTA DE AHORRO PROGRAMADO -");
        out.println("-------------------------------------------");

        out.print("\nIngrese el número de la cuenta corriente a la cual desea vincular la cuenta de ahorro programado: ");
        numCorriente = Long.parseLong(in.readLine());

        out.print("Ingrese un nuevo número de 7 dígitos que corresponda al número de cuenta de ahorro programado: ");
        numCuenta = Long.parseLong(in.readLine());

        out.print("Indique el monto que desea debitar de la cuenta corriente a la cuenta de ahorro programado mensualmente: ");
        montoDebitar = Double.parseDouble(in.readLine());

        String mensaje = BL.crearAhorroProg(numCorriente, numCuenta, montoDebitar, saldo, fechaApertura);
        System.out.println(mensaje);
    }

    /**
     * Metodo que lee el numero de cuenta y el saldo a depositar para una cuenta corriente
     * @param identificacion id para asociar el depósito con un usuario especifico
     * @throws IOException
     */
    static void realizarDeposito(long identificacion) throws IOException {
        long numCuenta;
        double saldo;

        out.println("------------------------------------");
        out.println("-----------  DEPÓSITOS  ------------");
        out.println("------------------------------------");

        out.print("\nIngrese el número de la cuenta en la cual desea realizar el depósito: ");
        numCuenta = Long.parseLong(in.readLine());

        out.print("\nIngrese el saldo a depositar en la cuenta. El saldo a depositar debe ser mayor a 0: ");
        saldo = Long.parseLong(in.readLine());

        BL.realizarDeposito(numCuenta, saldo, identificacion);
    }

    static void realizarDepositoAhorrP(long identificacion) throws IOException {
        long numCuenta;

        out.println("------------------------------------");
        out.println("-----------  DEPÓSITOS  ------------");
        out.println("------------------------------------");

        out.print("\nIngrese el número de la cuenta en la cual desea realizar el depósito: ");
        numCuenta = Long.parseLong(in.readLine());

        BL.realizarDepositoAhorrP(numCuenta, identificacion);
    }


    static void realizarRetiro(long identificacion) throws IOException { // el realizar retiro de la cuenta de ahorro programado debe tomar como parametro la fecha de apertura y numcuenta
        long numCuenta;
        double saldo;

        out.println("------------------------------------");
        out.println("------------  RETIROS  -------------");
        out.println("------------------------------------");

        out.print("\nIngrese el número de la cuenta en la cual desea realizar el retiro: ");
        numCuenta = Long.parseLong(in.readLine());

        out.print("\nIngrese el saldo a retirar en la cuenta. El saldo a retirar debe ser mayor a 0: ");
        saldo = Long.parseLong(in.readLine());

        BL.realizarRetiro(numCuenta, saldo, identificacion);
    }

   /* static void realizarRetiroAhorrP (long identificacion) throws IOException {
        long numCuenta;
        double montoRetiro;

        out.println("------------------------------------");
        out.println("------------  RETIROS  -------------");
        out.println("------------------------------------");

        out.print("\nIngrese el número de la cuenta en la cual desea realizar el retiro: ");
        numCuenta = Long.parseLong(in.readLine());

        out.print("\nIngrese el monto a retirar en la cuenta. El saldo a retirar debe ser mayor a 0: ");
        montoRetiro = Long.parseLong(in.readLine());

        BL.realizarRetiroAhorrP(numCuenta, montoRetiro, identificacion);
    }*/

    static void mostrarSaldo (long identificacion) throws IOException {
        long numCuenta;

        out.println("-------------------------------------");
        out.println("-------  SALDO DE LA CUENTA  --------");
        out.println("-------------------------------------");

        out.print("Ingrese el número de la cuenta para la cual desea revisar el saldo: ");
        numCuenta = Long.parseLong(in.readLine());

        BL.mostrarSaldo(numCuenta, identificacion);
    }

    static void mostrarCuentas() throws IOException {
        ArrayList<Cuenta> listaCuentas = BL.listarCuentas();
        for (int i = 0; i < listaCuentas.size(); i++) {
            Cuenta c;
            c = listaCuentas.get(i);
            System.out.println("Fecha de apertura de la cuenta: " +  c.getFechaApertura());
            System.out.println("Numero de cuenta: " + c.getNumCuenta());
            System.out.println("Saldo: " + c.getSaldo());
            System.out.println("Cliente asociado: " +  c.getDuennoCuenta());
        }
    }

    static void ingresoCliente() throws IOException {
        long identificacion;
        int cOpcion;
        int tipoOpcion;

        out.println("------------------------------------");
        out.println("-------  INGRESO AL SISTEMA  -------");
        out.println("------------------------------------");

        out.print("Ingrese su número de identificación para ingresar al sistema: ");
        identificacion = Long.parseLong(in.readLine());

        if (BL.ingresoCliente(identificacion)) {
            do {
                ingresoClienteMenu();
                cOpcion = seleccionarOpcion();
                switch (cOpcion) {
                    case 1:
                        crearCuentaMenu();
                        tipoOpcion = seleccionarOpcion();
                        switch (tipoOpcion) {
                            case 1:
                                crearCorriente(identificacion);
                                break;

                            case 2:
                                crearAhorro(identificacion);
                                break;

                            case 3:
                                crearAhorroProg();
                                break;

                            default:
                                System.out.print("Opción inválida. Por favor seleccione una opción del 1-3 listada en el menú\n");
                                break;
                        }
                        break;

                    case 2:
                        depositoMenu();
                        cOpcion = seleccionarOpcion();
                        switch (cOpcion) {
                            case 1:
                                realizarDeposito(identificacion);
                                break;

                            case 2:
                                realizarDepositoAhorrP(identificacion);
                                break;

                            default:
                                System.out.print("Opción inválida. Por favor seleccione una opción del 1-2 listada en el menú\n");
                                break;
                        }
                        break;

                    case 3:
                        realizarRetiro(identificacion);
                        break;

                    case 4:
                        mostrarSaldo(identificacion);
                        break;

                    case 5:
                        System.out.println("Volviendo al menú principal...\n");
                        break;

                    default:
                        System.out.print("Opción inválida. Por favor seleccione una opción del 1-5 listada en el menú\n");
                        break;
                }

            } while (cOpcion != 5);

        }
    }
}
