import java.time.LocalDate;

public class CuentaCorriente extends Cuenta {

    public CuentaCorriente() {

    }

    public CuentaCorriente(long numCuenta, double saldo, LocalDate fechaApertura, Cliente duennoCuenta) {
        super(numCuenta, saldo, fechaApertura, duennoCuenta);
    }

    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "numCuenta=" + getNumCuenta() +
                ", saldo=" + getSaldo() +
                ", fechaApertura=" + getFechaApertura() +
                ", duennoCuenta=" + getDuennoCuenta() +
                '}';
    }
}
