import java.time.LocalDate;

public class CuentaAhorroProg extends Cuenta {
    private CuentaCorriente cuentaAsociar;
    private double montoDebitar;
    private static final double tasaInteres = 0.02;

    public CuentaAhorroProg() {

    }

    public CuentaAhorroProg(long numCuenta, double saldo, LocalDate fechaApertura, Cliente duennoCuenta, CuentaCorriente cuentaAsociar, double montoDebitar) {
        super(numCuenta, saldo, fechaApertura, duennoCuenta);
        this.cuentaAsociar = cuentaAsociar;
        this.montoDebitar = montoDebitar;
    }

    public CuentaCorriente getCuentaAsociar() {
        return cuentaAsociar;
    }

    public void setCuentaAsociar(CuentaCorriente cuentaCorrAhorr) {
        this.cuentaAsociar = cuentaCorrAhorr;
    }

    public double getMontoDebitar() {
        return montoDebitar;
    }

    public void setMontoDebitar(double montoDebitar) {
        this.montoDebitar = montoDebitar;
    }

    public static double getTasaInteres() {
        return tasaInteres;
    }

    @Override
    public String toString() {
        return "CuentaAhorroProg{" +
                "numCuenta=" + getNumCuenta()  +
                ", saldo=" + getSaldo() +
                '}';
    }
}
