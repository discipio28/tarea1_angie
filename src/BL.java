import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class BL {

    private static ArrayList<Cliente> clientes = new ArrayList<>();
    private static ArrayList<Cuenta> cuentas = new ArrayList<>();
    private static ArrayList<CuentaCorriente> cuentasCorr = new ArrayList<>();
    private static ArrayList<CuentaAhorro> cuentasAhorr = new ArrayList<>();
    private static ArrayList<CuentaAhorroProg> cuentasAhorrProg = new ArrayList<>();

    /**
     * Registra los clientes en el sistema
     * @param nombre lee el nombre del cliente
     * @param identificacion lee la identificacion del cliente
     * @param fechaNacimiento lee la fecha de nacimiento del cliente
     * @param edad lee la edad del cliente
     * @param direccion lee la direccion del cliente
     * @return retorna si el cliente ya existe en la cuenta segun el id ingresado o si el cliente fue registrado con exito
     */
    public static String registrarClientes(String nombre, long identificacion, String fechaNacimiento, int edad, String direccion) { // Registra los clientes
        Cliente tmpCliente = new Cliente(nombre, identificacion, fechaNacimiento, edad, direccion);
        if (buscarCliente(identificacion)) {
            return "\nEste cliente ya existe";
        }
        clientes.add(tmpCliente);
        return "\nRegistro de cliente completado con éxito";
    }

    /**
     * Lista todos los clientes que existen en el sistema
     *
     * @return Retorna los clientes
     */
    public static ArrayList<Cliente> listarClientes() {
        return clientes;
    }

    /**
     * Lista todos los tipos de cuenta que existen en el sistema
     *
     * @return Retorna todas las cuentas
     */
    public static ArrayList<Cuenta> listarCuentas() {
        return cuentas;
    }

    /**
     * Lista todas las cuentas corrientes que existen en el sistema
     *
     * @return Retorna todas las cuentas corrientes
     */
    public static ArrayList<CuentaCorriente> listarCuentasCorr() {
        return cuentasCorr;
    }

    /**
     * Lista todas las cuentas de ahorro que existen en el sistema
     *
     * @return Retorna todas las cuentas de ahorro
     */
    public static ArrayList<CuentaAhorro> listarCuentasAhorr() {
        return cuentasAhorr;
    }

    /**
     * Lista todas las cuentas de ahorro programado que existen en el sistema
     *
     * @return Retorna todas las cuentas de ahorro programado
     */
    public static ArrayList<CuentaAhorroProg> listarCuentasAhorrP() {
        return cuentasAhorrProg;
    }

    /**
     * Lee el numero de cuenta
     *
     * @param numCuenta Retorna el numero de cuenta
     * @return
     */
    public static Long numCuentaID(long numCuenta) {
        return numCuenta;
    }

    /***
     * Recorre la lista de clientes registrados
     * @param identificacion Busca a los clientes segun el numero de identificacion con el que se registraron
     * @return retorna un booleano que indica si dicho cliente existe o no con la identificacion ingresada
     */
    public static boolean buscarCliente(long identificacion) { // Busca los clientes registrados
        for (int i = 0; i < clientes.size(); i++) {
            Cliente c = clientes.get(i);
            if (Objects.equals(c.getIdentificacion(), identificacion)) {
                return true;
            }
        }
        return false;
    }

    /***
     * Recorre la lista de todas las cuentas registradas
     * @param numCuenta Recorre una lista de todas las cuentas segun el numero de cuenta con el que esten registrados
     * @return retorna un booleano que indica si dicha cuenta existe o no con el numero de cuenta ingresado
     */
    public static boolean buscarCuenta(long numCuenta) { // Busca las cuentas de los clientes
        for (int i = 0; i < cuentas.size(); i++) {
            Cuenta c = cuentas.get(i);
            if (Objects.equals(c.getNumCuenta(), numCuenta)) {
                return true;
            }
        }
        return false;
    }

    /***
     * Recorre la lista de todas las cuentas corrientes registradas
     * @param numCuenta Recorre una lista de todas las cuentas corrientes segun el numero de cuenta con el que esten registrados
     * @return retorna un booleano que indica si dicha cuenta existe o no con el numero de cuenta ingresado
     */
    public static boolean buscarCuentaCorr(long numCuenta) { // Busca las cuentas de los clientes
        for (int i = 0; i < cuentasCorr.size(); i++) {
            CuentaCorriente c = cuentasCorr.get(i);
            if (Objects.equals(c.getNumCuenta(), numCuenta)) {
                return true;
            }
        }
        return false;
    }

    /***
     * Recorre la lista de todas las cuentas de ahorro registradas
     * @param numCuenta Recorre una lista de todas las cuentas de ahorro segun el numero de cuenta con el que esten registrados
     * @return retorna un booleano que indica si dicha cuenta existe o no con el numero de cuenta ingresado
     */
    public static boolean buscarCuentaAhorr(long numCuenta) { // Busca las cuentas de los clientes
        for (int i = 0; i < cuentasAhorr.size(); i++) {
            CuentaAhorro c = cuentasAhorr.get(i);
            if (Objects.equals(c.getNumCuenta(), numCuenta)) {
                return true;
            }
        }
        return false;
    }

    /***
     * Recorre la lista de todas las cuentas de ahorro registradas
     * @param numCuenta Recorre una lista de todas las cuentas de ahorro programado segun el numero de cuenta con el que esten registrados
     * @return retorna un booleano que indica si dicha cuenta existe o no con el numero de cuenta ingresado
     */
    public static boolean buscarCuentaAhorrP(long numCuenta) { // Busca las cuentas de los clientes
        for (int i = 0; i < cuentasAhorrProg.size(); i++) {
            CuentaAhorroProg c = cuentasAhorrProg.get(i);
            if (Objects.equals(c.getNumCuenta(), numCuenta)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Booleano que busca a los clientes registrados y si existen se les indica que lograron ingresar al sistema, de lo contrario indica que no existe tal usuario
     *
     * @param identificacion Lee el numero de identificacion que usa el cliente para intentar ingresar al sistema
     * @return Retorna true si el usuario existe con el numero de identificacion y false si no existe
     */
    public static Boolean ingresoCliente(long identificacion) {
        if (buscarCliente(identificacion)) {
            System.out.println("\nUsuario " + identificacion + " ingresado al sistema con éxito.");
            return true;
        } else {
            System.out.println("\nNo existe un usuario con el número de identificación " + identificacion + ".");
            return false;
        }
    }

    /**
     * Crea una cuenta corriente y agrega la cuenta al arreglo de todas las cuentas y cuentas corrientes
     *
     * @param numCuenta      lee el numero de cuenta ingresado por el usuario y verifica si este existe
     * @param saldo          lee el saldo ingresado por el usuario y verifica si este cumple con las condiciones para la apertura de la cuenta corriente
     * @param identificacion lee el numero de identificacion ingresado
     * @param fechaApertura  lee la fecha de apertura de la cuenta corriente
     * @return retorna las validaciones que requiere la creacion de la cuenta corriente y si todas las validaciones pasan, retorna un String que indica que la cuenta se creo con exito
     */
    public static String crearCorriente(long numCuenta, double saldo, long identificacion, LocalDate fechaApertura) { // Crea una cuenta corriente
        if (!(saldo >= 50000) && (String.valueOf(numCuenta).length() != 7)) {
            return "\n El número de la cuenta y el saldo mínimo no cumplen con los requisitos para la creación de la cuenta. Por favor revise los datos ingresados e intente nuevamente";
        } else if (!(saldo >= 50000)) {
            return "\nEl saldo de la cuenta debe ser igual o mayor que 50.000, por favor intente nuevamente.";
        } else if (String.valueOf(numCuenta).length() != 7) {
            return "\nEl número de la cuenta debe ser de 7 dígitos, por favor intente nuevamente.";
        } else if (buscarCuenta(numCuenta)) {
            return "\nEsta cuenta ya existe, por favor intente nuevamente.";
        }

        for (int j = 0; j < clientes.size(); j++) {
            Cliente c = clientes.get(j);
            if (Objects.equals(c.getIdentificacion(), identificacion)) {
                CuentaCorriente cuentaCorr = new CuentaCorriente(numCuenta, saldo, fechaApertura, c);
                System.out.println("\nNúmero de cuenta: " + numCuentaID(numCuenta) + "\nDueño de la cuenta: " + cuentaCorr.getDuennoCuenta() + "\nFecha de apertura: " + fechaApertura);
                cuentas.add(cuentaCorr);
                cuentasCorr.add(cuentaCorr);
            }
        }
        return "\nCuenta corriente creada con éxito";
    }

    public static String crearAhorro(long numCuenta, double saldo, long identificacion, LocalDate fechaApertura) {
        if (!(saldo >= 50000) && (String.valueOf(numCuenta).length() != 7)) {
            return "\n El número de la cuenta y el saldo mínimo no cumplen con los requisitos para la creación de la cuenta. Por favor revise los datos ingresados e intente nuevamente";
        } else if (!(saldo >= 50000)) {
            return "\nEl saldo de la cuenta debe ser igual o mayor que 50.000, por favor intente nuevamente.";
        } else if (String.valueOf(numCuenta).length() != 7) {
            return "\nEl número de la cuenta debe ser de 7 dígitos, por favor intente nuevamente.";
        } else if (buscarCuenta(numCuenta)) {
            return "\nEsta cuenta ya existe, por favor intente nuevamente.";
        }

        for (int j = 0; j < clientes.size(); j++) {
            Cliente c = clientes.get(j);
            if (Objects.equals(c.getIdentificacion(), identificacion)) {
                CuentaAhorro cuentaAhorr = new CuentaAhorro(numCuenta, saldo, fechaApertura, c);
                System.out.println("\nNúmero de cuenta: " + numCuentaID(numCuenta) + "\nDueño de la cuenta: " + cuentaAhorr.getDuennoCuenta() + "\nFecha de apertura: " + fechaApertura);
                cuentas.add(cuentaAhorr);
                cuentasAhorr.add(cuentaAhorr);
            }
        }
        return "\nCuenta de ahorro creada con éxito";
    }

    public static String crearAhorroProg(long numCorriente, long numCuenta, double montoDebitar, double saldo, LocalDate fechaApertura) { // Crea una cuenta de ahorro programado
        if (buscarCuenta(numCuenta)) {
            return "\nEsta cuenta ya existe, por favor intente nuevamente.";
        } else if (String.valueOf(numCuenta).length() != 7) {
            return "\nEl número de la cuenta debe ser de 7 dígitos, por favor intente nuevamente.";
        } else if (buscarCuentaCorr(numCorriente)){
            for(int i = 0; i < cuentasCorr.size(); i++) {
                CuentaCorriente cc = cuentasCorr.get(i);
                if (Objects.equals(cc.getNumCuenta(),numCorriente)) {
                    Cliente c = cc.getDuennoCuenta();
                    CuentaAhorroProg cuentaAhorroP = new CuentaAhorroProg(numCuenta, saldo, fechaApertura, c, cc, montoDebitar);
                    System.out.println("\nNúmero de cuenta: " + numCuentaID(numCuenta) + "\nDueño de la cuenta: " + cuentaAhorroP.getDuennoCuenta() + "\nFecha de apertura: " + fechaApertura);
                    cuentas.add(cuentaAhorroP);
                    cuentasAhorrProg.add(cuentaAhorroP);
                }
            }
        } else {
            return "\nNo existe una cuenta corriente con el número ingresado, por favor intente nuevamente.";
        }
        System.out.println(cuentasAhorrProg);
        return "\nCuenta de ahorro programado creada con éxito";
    }

    public static void realizarDeposito(long numCuenta, double nuevoSaldo, long identificacion) { // Realiza depósitos a las cuentas existentes
        LocalDate fechaMovimiento = LocalDate.now();
        double tasaInteres;

        if (nuevoSaldo <= 0) {
            System.out.println("\nEl monto a depositar no es un monto válido, por favor ingrese un monto mayor a 0.");
        } else if (buscarCuentaCorr(numCuenta)) {
            for (int i = 0; i < cuentasCorr.size(); i++) {
                CuentaCorriente cc = cuentasCorr.get(i);
                if (cc.getNumCuenta() == numCuenta) {
                    if (cc.getDuennoCuenta().getIdentificacion() == identificacion) {
                        cc.setSaldo(cc.getSaldo()+nuevoSaldo);
                        movimientoCuentas(fechaMovimiento, nuevoSaldo, true);
                        System.out.println("\nSaldo actual en cuenta corriente " + cc.getNumCuenta() + ": " + cc.getSaldo());
                    } else {
                        System.out.println("\nEl número de cuenta ingresado no existe para este cliente. Por favor ingrese un número de cuenta válido e intente el depósito nuevamente.");
                    }
                }
            }
        } else if (buscarCuentaAhorr(numCuenta)) {
            for (int i = 0; i < cuentasAhorr.size(); i++) {
                CuentaAhorro ca = cuentasAhorr.get(i);
                if (ca.getNumCuenta() == numCuenta) {
                    if (ca.getDuennoCuenta().getIdentificacion() == identificacion) {
                        tasaInteres = nuevoSaldo * CuentaAhorro.getTasaInteres();
                        ca.setSaldo(ca.getSaldo()+(nuevoSaldo - tasaInteres));
                        movimientoCuentas(fechaMovimiento, nuevoSaldo, true);
                        System.out.println("\nSaldo actual en cuenta de ahorro " + ca.getNumCuenta() + ": " + ca.getSaldo());
                    } else {
                        System.out.println("\nEl número de cuenta ingresado no existe para este cliente. Por favor ingrese un número de cuenta válido e intente el depósito nuevamente.");
                    }
                }
            }
        } else {
            System.out.println("\nEl número de cuenta ingresado no existe. Por favor ingrese un número de cuenta válido e intente el depósito nuevamente.");
        }
    }

    public static void realizarDepositoAhorrP (long numCuenta, long identificacion) {
        double tasaInteres;
        LocalDate fechaHoy = LocalDate.now();
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd");

         if (buscarCuentaAhorrP(numCuenta)) {
            for (int i = 0; i < cuentasAhorrProg.size(); i++) {
                CuentaAhorroProg cap = cuentasAhorrProg.get(i);
                if (cap.getNumCuenta() == numCuenta) {
                    if (cap.getDuennoCuenta().getIdentificacion() == identificacion) {
                        if (cap.getCuentaAsociar().getSaldo() > cap.getMontoDebitar()) {
                            if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == 14) { // REMEMBER TO CHANGE THIS BEFORE SUBMITTING TO 1
                                tasaInteres = cap.getMontoDebitar() * CuentaAhorroProg.getTasaInteres();
                                cap.getCuentaAsociar().setSaldo(cap.getCuentaAsociar().getSaldo()-cap.getMontoDebitar());
                                cap.setSaldo(cap.getSaldo()+(cap.getMontoDebitar() - tasaInteres));
                                movimientoCuentas(fechaHoy, cap.getMontoDebitar(), true);
                                System.out.println("\nSaldo actual en cuenta de ahorro programado " + cap.getNumCuenta() + ": " + cap.getSaldo());
                            } else {
                                System.out.println("Recuerde que solo puede realizar depósitos los 1 de cada mes, la fecha de hoy es: " + formatdate.format(date));
                            }
                        } else {
                            System.out.println("Fondos insuficientes en cuenta corriente, no se puede realizar el depósito en este momento.");
                        }
                    } else {
                        System.out.println("Id mismatch");
                    }
                } else {
                    System.out.println("Acc # mismatch");
                }
            }
        } else {
             System.out.println("No se ha encontrado una cuenta con el número ingresado, por favor revise el número de cuenta e intente nuevamente.");
         }
    }

    public static void realizarRetiro(long numCuenta, double nuevoSaldo, long identificacion) {
        LocalDate fechaMovimiento = LocalDate.now();
        if (buscarCuentaCorr(numCuenta)) {
            for (int i = 0; i < cuentasCorr.size(); i++) {
                CuentaCorriente cc = cuentasCorr.get(i);
                if (cc.getNumCuenta() == numCuenta) {
                    if (cc.getDuennoCuenta().getIdentificacion() == identificacion) {
                        if (nuevoSaldo <= 0) {
                            System.out.println("\nEl monto a retirar no es un monto válido, por favor ingrese un monto mayor a 0.");
                        } else {
                            cc.setSaldo(cc.getSaldo()-nuevoSaldo);
                            movimientoCuentas(fechaMovimiento, nuevoSaldo, false);
                            System.out.println("\nSaldo actual en cuenta corriente " + cc.getNumCuenta() + ": " + cc.getSaldo());
                        }
                    } else {
                        System.out.println("\nEl número de cuenta ingresado no existe para este cliente. Por favor ingrese un número de cuenta válido e intente el retiro nuevamente.");
                    }
                }
            }
        } else if (buscarCuentaAhorr(numCuenta)) {
            for (int i = 0; i < cuentasAhorr.size(); i++) {
                CuentaAhorro ca = cuentasAhorr.get(i);
                if (ca.getNumCuenta() == numCuenta) {
                    if (ca.getDuennoCuenta().getIdentificacion() == identificacion) {
                        if (nuevoSaldo <= 0) {
                            System.out.println("\nEl monto a retirar no es un monto válido, por favor ingrese un monto mayor a 0.");
                        } else if (((nuevoSaldo > (ca.getSaldo() * 0.50)))) {
                            System.out.println("\nEl monto a retirar no es un monto válido, por favor ingrese un monto menor al 50% saldo actual de la cuenta.");
                        } else if (!(nuevoSaldo >= 100000)) {
                            System.out.println("\nEl monto a retirar no es un monto válido, por favor ingrese un monto mayor o igual a 100.000 colones.");
                        } else {
                            ca.setSaldo(ca.getSaldo()-nuevoSaldo);
                            System.out.println("\nSaldo actual en cuenta de ahorro " + ca.getNumCuenta() + ": " + ca.getSaldo());
                            movimientoCuentas(fechaMovimiento, nuevoSaldo, false);
                        }
                    } else {
                        System.out.println("\nEl número de cuenta ingresado no existe para este cliente. Por favor ingrese un número de cuenta válido e intente el retiro nuevamente.");
                    }
                }
            }
        } else if (buscarCuentaAhorrP(numCuenta)) {
            for (int i = 0; i < cuentasAhorrProg.size(); i++) {
                CuentaAhorroProg cap = cuentasAhorrProg.get(i);
                if (cap.getNumCuenta() == numCuenta) {
                    if (cap.getDuennoCuenta().getIdentificacion() == identificacion) {
                        int periodo = Period.between(cap.getFechaApertura(), fechaMovimiento).getYears();
                        if (nuevoSaldo <= 0) {
                            System.out.println("\nEl monto a retirar no es un monto válido, por favor ingrese un monto mayor a 0.");
                        } else if (nuevoSaldo > cap.getSaldo()) {
                            System.out.println("\nEl monto a retirar no es un monto válido, por favor ingrese un monto menor al saldo actual de la cuenta.");
                        } else if (periodo >= 1) {
                            cap.setSaldo(cap.getSaldo()-nuevoSaldo);
                            System.out.println("\nSaldo actual en cuenta de ahorro " + cap.getNumCuenta() + ": " + cap.getSaldo());
                            movimientoCuentas(fechaMovimiento, nuevoSaldo, false);
                        } else {
                            System.out.println("\nAún no ha pasado 1 año desde la fecha de apertura de la cuenta. Recuerde que debe esperar 1 año antes de poder realizar el retiro.");
                        }
                    } else {
                        System.out.println("\nEl número de cuenta ingresado no existe para este cliente. Por favor ingrese un número de cuenta válido e intente el retiro nuevamente.");
                    }
                }
            }
        } else {
            System.out.println("\nEl número de cuenta ingresado no existe. Por favor ingrese un número de cuenta válido e intente el retiro nuevamente.");
        }
    }

    public static void mostrarSaldo (long numCuenta, long identificacion) { // Muestra el saldo de las cuentas ingresadas
        if (buscarCuenta(numCuenta)) {
            for (int i = 0; i < cuentas.size(); i++) {
                Cuenta c = cuentas.get(i);
                if (Objects.equals(c.getNumCuenta(), numCuenta)) {
                    if (c.getSaldo() <= 0) {
                        System.out.println("Cuenta no tiene fondos");
                    } else if (c.getSaldo() > 0) {
                        System.out.println("El saldo de la cuenta es de: " + c.getSaldo());
                    }
                }
            }
        }
    }

    public static void movimientoCuentas(LocalDate fechaMovimiento, Double montoMovimiento, Boolean tipoMovimiento) {
        String descripcionMovimiento;

        if (tipoMovimiento == true) {
            descripcionMovimiento = "Depósito";
        } else {
            descripcionMovimiento =  "Retiro";
        }

        System.out.println("Fecha del movimiento: " + (fechaMovimiento));
        System.out.println("Descripción: " + descripcionMovimiento);
        System.out.println("Monto del movimiento: " + montoMovimiento);
    }

}