import java.time.LocalDate;

public class CuentaAhorro extends Cuenta {
    private static final double tasaInteres = 0.02;

    public CuentaAhorro() {

    }

    public CuentaAhorro(long numCuenta, double saldo, LocalDate fechaApertura, Cliente duennoCuenta) {
        super(numCuenta, saldo, fechaApertura, duennoCuenta);
    }

    public static double getTasaInteres() {
        return tasaInteres;
    }

    @Override
    public String toString() {
        return "CuentaAhorro{" +
                "numCuenta=" + getNumCuenta() +
                ", saldo=" + getSaldo() +
                '}';
    }
}
